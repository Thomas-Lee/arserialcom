//Serial communications protocol - Thomas Lee 2013 test

#include <Arduino.h>

//Declare functions

void serialRX();
void serialTXPosition();
void serialRXReady();
void errorLED();
void readyLED();
void comLED();

//RX
char bufferRX[20]; //Buffer array size
char bufferCharRX; //Char read from serial
int bufferIndexRX; //index in buffer to write to

//Status of serial
enum e_serialRXStatus {
	RX_READY,RX_COMMAND,RX_DATA
};
e_serialRXStatus serialRXStatus;

enum e_serialTXStatus {
	TX_READY,TX_WAITING
};
e_serialTXStatus serialTXStatus;

//RX Type of command in serial read
enum e_serialCommand {
	A, B
};
e_serialCommand serialCommand;

//Strtok
const char* delimiterRX = ",";
const char* commandRX[2]; //command size
char* strPointerRX;

//TX
char commandTX[40]; //Command string size buffer
int commandStrSizeTX;


void setup() {

	Serial.begin(9600);
	serialRXStatus = RX_READY;
	serialTXStatus = TX_READY;
	pinMode(13, OUTPUT);

}

void loop() {

	serialRX();

}

void serialRX() {

	while (Serial.available() > 0) {

		bufferCharRX = Serial.read();

		switch (serialRXStatus) {

		case RX_READY: //----------------------------------------------------------

			switch (bufferCharRX) {

				case 1:
					serialRXStatus = RX_COMMAND;
					comLED();
					break;
				case 6:
					serialTXStatus = TX_READY;
					comLED();
					break;

				default:
					errorLED();
					break;
			}
			break;

		case RX_COMMAND: //----------------------------------------------------------

			switch (bufferCharRX) {

				case 'A':
					serialCommand = A;
					serialRXStatus = RX_DATA;
					comLED();
					break;

				case 'B':
					serialCommand = B;
					serialRXStatus = RX_DATA;
					comLED();
					break;
				default:
					errorLED();
					break;
			}
			break;

		case RX_DATA:   //------------------------------------------------------

			switch (serialCommand) {

				case A:

					if (bufferCharRX != 4) {
						bufferRX[bufferIndexRX] = bufferCharRX;
						bufferIndexRX++;
						comLED();
					} else {

						//Include terminating NULL in buffer end - strtok looks for it to stop
						bufferRX[bufferIndexRX] = '\0';
						//StrTok delimit buffer
						strPointerRX = strtok(bufferRX, delimiterRX);
						for (int i = 0; i < 2; i++) { //Go sthrough 2 sections - command,command
							commandRX[i] = strPointerRX;
							strPointerRX = strtok(NULL, delimiterRX);
						}

						bufferIndexRX = 0;

						serialTXPosition();
						serialRXReady();


					}

				break;

				case B:

					if (bufferCharRX != 4) {
						bufferRX[bufferIndexRX] = bufferCharRX;
						bufferIndexRX++;
						comLED();
					} else {

						//Include terminating NULL in buffer end - strtok looks for it to stop
						bufferRX[bufferIndexRX] = '\0';
						//StrTok delimit buffer
						strPointerRX = strtok(bufferRX, delimiterRX);
						for (int i = 0; i < 2; i++) { //Go sthrough 2 sections - command,command
							commandRX[i] = strPointerRX;
							strPointerRX = strtok(NULL, delimiterRX);
						}

						bufferIndexRX = 0;

						digitalWrite(atoi(commandRX[0]), HIGH);
						delay(atoi(commandRX[1]));
						digitalWrite(atoi(commandRX[0]), LOW);

						serialRXReady();
					}

				break;

				default:
					errorLED();
					break;

			}//serial_command switch end
			break;



		default: //------------------------------------------------------
			digitalWrite(13,HIGH);
			delay(100);
			digitalWrite(13,LOW);
		break;

		} //Main switch end

		break;

	}
}



void serialTXPosition() {
	if(serialTXStatus == TX_READY){
		commandStrSizeTX = sprintf(commandTX, "%c%c%i,%i%c", 1, 'A', 111, 222, 4);
		Serial.write((unsigned char*) commandTX, commandStrSizeTX);
		serialTXStatus = TX_WAITING;
	}
}


void serialRXReady() {
	Serial.write(6);
	serialRXStatus = RX_READY;
}

void errorLED(){
	bool ledState = true;
	for(int i=0; i<10; i++){
		digitalWrite(13,ledState);
		ledState=!ledState;
		delay(100);
	}
}

void readyLED(){
	bool ledState = true;
	for(int i=0; i<2; i++){
		digitalWrite(13,ledState);
		ledState=!ledState;
		delay(500);
	}
}

void comLED(){
	bool ledState = true;
	for(int i=0; i<2; i++){
		digitalWrite(13,ledState);
		ledState=!ledState;
		delay(25);
	}
}

